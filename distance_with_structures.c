//WAP to find the distance between two points using structures and 4 functions.
#include<stdio.h>
#include<math.h>
struct Points 
 {
  float x,y;
 };
float find_distance(struct Points a,struct Points b)
 {
   float distance;
   distance=sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
   return distance;
 }
int main()
 {
  struct Points a,b;
   printf("Enter the coordinates of point 1:\n");
   printf("Enter X axis coordinates:\n");
   scanf("%f",&a.x);
   printf("Enter Y axis coordinates:\n");
   scanf("%f",&a.y);
     printf("Enter the coordinates of point 2:\n");
   printf("Enter X axis coordinates:\n");
   scanf("%f",&b.x);
   printf("Enter Y axis coordinates:\n");
   scanf("%f",&b.y);
  printf("Distance between point1 and point2:%f\n",find_distance(a,b));
  return 0;
}
