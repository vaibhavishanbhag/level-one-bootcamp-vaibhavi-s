//WAP to find the volume of a tromboloid using 4 functions.
#include<stdio.h>
float input()
 {
  float x;
  scanf("%f",&x);
  return x;
 }
float volume(float h,float d,float b)
 {
  float vol=1.0/3.0 * ((h*d*b)+(d/b));
  return vol;
 }
void output(float h,float d,float b)
 {
  printf("volume of tromboloid=%f\n",volume(h,d,b));
 }
float main()
 {
  float h,d,b;

  printf("Enter the value of h:\n");
  h=input();
  printf("Enter the value of d:\n");
  d=input();
  printf("Enter the value of b:\n");
  b=input();
  output(h,d,b);
  return 0;
 }
