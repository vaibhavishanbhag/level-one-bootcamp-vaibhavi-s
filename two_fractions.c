//WAP to find the sum of two fractions.
#include<stdio.h>
typedef struct fraction
{
int num;
int deno;
}Fraction;

Fraction input()
{
Fraction g;
printf("Enter the num:\n");
scanf("%d",&g.num);
printf("\n Enter the deno:\n");
scanf("%d",&g.deno);
return g;
}
int hcf(int a,int b)
{
int i,gcd=1;
for(i=0; i<=a && i<=b; ++i)
{
if(a%i==0 && b%i==0)
gcd=i;
}
return gcd;
}

Fraction sum(Fraction g1, Fraction g2)
{
int gcd;
	Fraction temp;
	temp.num=((g1.num*g2.deno)+(g2.num*g1.deno));
	temp.deno=(g1.deno*g2.deno);
	gcd = hcf(temp.num, temp.deno);
	temp.num=temp.num/gcd;
	temp.deno=temp.deno/gcd;
	return temp;
}
void result( Fraction g1, Fraction g2, Fraction sum)
{
printf("The sum of %d/%d +                              %d/%d                                                                                             is       %d/%d",g1.num,g1.deno,g2.num,g2.deno,sum.num,sum.deno);
}
int main()
{
Fraction g1,g2,res;
printf("Enter the num and deno of First fraction:");
g1=input();
printf("Enter the num and deno of Second function:");
g2=input();
res=sum(g1,g2);
result(g1,g2,res);
return 0;
}
